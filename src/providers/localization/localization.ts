import { HttpClient, HttpHeaders, HttpParams, HttpRequest, HttpEvent, HttpEventType } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Platform } from 'ionic-angular';
import { Observable } from 'rxjs/Observable';
import { Geolocation, Geoposition } from '@ionic-native/geolocation'
import { retry, catchError, tap, map } from 'rxjs/operators';
import * as Parser from 'fast-xml-parser';

import { Address } from './../../models/address';
import { UF } from '../../models/unidades-federativas';
// import { NgxXml2jsonService } from 'ngx-xml2json';

/*
  Generated class for the LocalizationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class LocalizationProvider {

  private position;
  private readonly uriReverseGeo: string = 'https://nominatim.openstreetmap.org/reverse';

  constructor(
    private http: HttpClient,
    public platform: Platform,
    private geolocation: Geolocation
  ) {
    console.log('Hello LocalizationProvider Provider');
    this.position = null;
  }
  getCurrentPosition(sucessCallback, errorCallback, options?: PositionOptions): void {
    this.platform.ready().then(() => {
      this.geolocation.getCurrentPosition(options).then((pos) => {
        sucessCallback(pos);
      }).catch(error => {
        errorCallback(error);
      })
    }).catch(errorCallback);
  }

  watchPosition(sucessCallBack?, errorCallBack?, options?): Observable<Geoposition> {
    let pos$: Observable<Geoposition> = null;
    this.platform.ready().then(() => {
      pos$ = this.geolocation.watchPosition(options);
      if (sucessCallBack) {
        sucessCallBack(pos$);
      }
    }).catch(error => {
      if (errorCallBack) {
        errorCallBack(error)
      }
    });
    return pos$;
  }

  reverseGeocoding(position): Observable<Address> {
    const _params: HttpParams = new HttpParams().set("lat", `${position.coords.latitude}`).set("lon", `${position.coords.longitude}`);
    return this.httpGetXMLContentRequest(new URL('https://nominatim.openstreetmap.org/reverse'), _params)
      .pipe(
        retry(3),
        map(
          httpResponse => {
            if (!(httpResponse in HttpEventType)) {
              const json = Parser.parse(httpResponse).reversegeocode.addressparts;
              return new Address({
                name: json.road,
                suburb: json.suburb,
                state_district: json.state_district,
                state: json.state,
                postcode: json.postcode,
                country: json.country,
                country_short: json.country_code.toUpperCase(),
                city: json.city,
                geoposition: [position.coords.latitude, position.coords.longitude]
              });
            }else{
              return undefined;
            }
          }
        ));

  }
  findAddressByPostCode(_postcode:string):Observable<Address>{
    let postcode = _postcode.trim().replace(/[^\d]+/g, "");
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Date': `${(new Date()).toLocaleString()}`
      })
    }
    const url:string = `http://viacep.com.br/ws/${postcode}/json/`;
    return this.http.get<Address>(url,httpOptions).pipe(
      map((response) => {
        let aux:string = '';
        const address:Address = new Address() || undefined;
        address.name = response['logradouro'] || undefined;
        address.suburb = response['bairro'] || undefined;
        address.city = response['localidade'] || undefined;
        address.state_short = response['uf'] || undefined;
        address.state = UF[address.state_short] || undefined;
        aux = response['cep'];
        if(aux !== undefined && aux !== '' && aux !== null){
          aux = aux.trim().replace(/[^\d]+/g, "") ;
        }else{
          aux = undefined;
        }
        address.postcode = aux;
        return address;
      })
    );
  }
  httpGetXMLContentRequest(url: URL, _params?: HttpParams): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'text/xml',
        'Accept': 'text/xml'
      }),
      params: _params,
      responseType: 'text'
    }
    let httpR: HttpRequest<any> = new HttpRequest<any>(
      "GET", url.href, httpOptions);
    try {
      return this.http.request(httpR).pipe(
        map((httpEvent: HttpEvent<any>) => {
          if (httpEvent.hasOwnProperty('body')) {
            return httpEvent['body'];
          } else if (httpEvent.type == HttpEventType.Response) {
            return undefined;
          }
          else {
            return httpEvent.type;
          }
        })
      );
    } catch (error) {
      return Observable.throw(error);
    }
  }
  convertUTCDateToLocalDate(date) {
    const newDate = new Date(date.getTime() + date.getTimezoneOffset() * 60 * 1000);
    const offset = date.getTimezoneOffset() / 60;
    const hours = date.getHours(); 
    newDate.setHours(hours - offset);
    return newDate;
}
}



