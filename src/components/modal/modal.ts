import { Address } from './../../models/address';
import { Component, Input } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { ViewController, NavParams } from 'ionic-angular';

/**
 * Generated class for the ModalComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'modal',
  templateUrl: 'modal.html'
})
export class ModalComponent {

  @Input()
  address: Address;

  constructor(public viewCtrl: ViewController, public params: NavParams) {
    console.log('Hello ModalComponent Component');
    this.address = params.get('address') || undefined;
  }
  dismiss() {
    this.viewCtrl.dismiss();
  }

}
