import { NgModule } from '@angular/core';
import { ModalComponent } from './modal/modal';
import { IonicModule } from 'ionic-angular';
@NgModule({
	declarations: [ModalComponent],
	imports: [IonicModule],
	exports: [ModalComponent]
})
export class ComponentsModule {}
