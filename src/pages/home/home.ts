import { Component } from '@angular/core';
import { NavController, Platform, ModalController } from 'ionic-angular';
import { LocalizationProvider } from '../../providers/localization/localization';
import { HttpClient} from '@angular/common/http';

import { Address } from './../../models/address';
import { ModalComponent } from '../../components/modal/modal';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
//https://opencagedata.com/credits
export class HomePage {

  address:Address;
  constructor(
    public navCtrl: NavController, 
    public local:LocalizationProvider, 
    public modalCtrl: ModalController,
    private http: HttpClient 
  ) {
   
  }
  ionViewDidLoad(){

  }
  openModal(num:number){
    switch (num) {
      case 0:
        this.local.getCurrentPosition(pos => {
          this.local.reverseGeocoding(pos).subscribe( address =>{
            console.log(JSON.stringify(address));
          })
        }, error => {
          console.log(error);
        });
        let modal = this.modalCtrl.create(ModalComponent);
    modal.present();
        break;
      case 1:
        this.local.findAddressByPostCode('59147-398').subscribe(
          (address) => {
            console.log(address);
          }
        );
      default:
        break;
    }
  }

}
