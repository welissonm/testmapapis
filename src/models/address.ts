import { city } from "./city";

export class Address{
    name:string;
    number:number;
    complement_number:string;
    complement:string;
    suburb:string
    state_district:string;
    state:string;
    state_short:string;//abreviacao do estado
    postcode:string;
    country:string;
    country_short:string;//abreviacao do pais
    city:city
    geoposition:Array<number>;
    constructor(json?:Object){
        if(json){
            this.name = json['name'] || undefined;
            this.number = json['number'] || undefined;
            this.complement_number = json['complement_number'] || undefined;
            this.complement = json['complement'] || undefined;
            this.suburb = json['suburb'] || undefined;
            this.postcode = json['postcode'] || undefined;
            this.city = json['city'] || undefined;
            this.state_district = json['state_district'] || undefined;
            this.state = json['state'] || undefined;
            this.state_short = json['state_short'] || undefined;
            this.country = json['country'] || undefined;
            this.country_short = json['country_short'] || undefined;
            this.geoposition = json['geoposition'] || undefined;
        }
    }
    formattedAddress():string{
        let formarted:string = ''
        return formarted;
    }
}